Allows entity reference fields imported by feeds to append to the existing values rather than overwrite.

Especially useful when you want multiple sources to update the same set of nodes (requires the following patch - https://drupal.org/node/1539224#comment-8375315).

Instructions for Use

 - Enable the module.
 - A new checkbox option will appear when you map a field of type entity reference, which you alows you to switch between 'append' and 'overwrite'.  When append is selected, any existing values will be retained and new values will be appended.  Duplicates will be removed automatically.

